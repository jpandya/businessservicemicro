package com.jiten.innovations.springCloud.BusinessServiceMicro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BusinessServiceMicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusinessServiceMicroApplication.class, args);
	}

}
